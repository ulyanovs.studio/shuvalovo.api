const schema = require('../models/Schema');

// start of use mongoose DB SCHEME
const schemaUsers = schema.User;

let noAuth = res => {
  res.status(401).json({ error: 'User authorization failed' });
};

let workWithHeaders = req => {
  return new Promise((resolve, reject) => {
    let cookies = [];
    let token;
    if (req.headers) {
      if (req.headers.cookie) {
        cookies = req.headers.cookie.split(' ');
      }
      if (req.headers.cookies) {
        cookies = req.headers.cookies.split(' ');
      }
    }
    if (cookies.length) {
      let index = cookies.findIndex(item => item.includes('__a_t'));
      let cookiesToken = cookies[index].split('=')[1];
      token = cookiesToken.substr(0, cookiesToken.length - 1);
    }
    if (token) {
      schemaUsers
        .findOne({ access_token: token })
        .then(user => {
          if (user) {
            resolve(user);
          } else {
            reject();
          }
        })
        .catch(() => {
          reject();
        });
    } else {
      reject();
    }
  });
};

module.exports.workWithHeaders = workWithHeaders;

module.exports.admin = function(req, res, next) {
  workWithHeaders(req)
    .then(user => {
      if (user.role === 'admin' && user.active) {
        next();
      } else {
        noAuth();
      }
    })
    .catch(() => {
      noAuth(res);
    });
};

module.exports.allUser = function(req, res, next) {
  workWithHeaders(req)
      .then(user => {
        if (user && user.active) {
          next();
        } else {
          noAuth();
        }
      })
      .catch(() => {
        noAuth(res);
      });
};
