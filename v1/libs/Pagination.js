module.exports.paginationData = (data, query, count, dataName) => {
  let paginationData = {};
  let requestPagination = query.pagination
    ? eval(`(${query.pagination})`)
    : { perPage: 20, currentPage: 1 };
  let perPage = requestPagination.perPage;
  let responsePagination = {
    perPage: perPage,
    currentPage: requestPagination.currentPage,
    totalPageCount: count
  };
  paginationData[dataName] = data;
  paginationData.pagination = responsePagination;
  return paginationData;
};

module.exports.sortAndPagination = data => {
  let dataParseObj = {};
  let pagination;
  dataParseObj.sort = data.sort ? eval(`(${data.sort})`) : { id: -1 };
  pagination = data.pagination
    ? eval(`(${data.pagination})`)
    : { perPage: 20, currentPage: 1 };
  dataParseObj.perPage = pagination.perPage;
  dataParseObj.page = pagination.currentPage;
  return dataParseObj;
};
