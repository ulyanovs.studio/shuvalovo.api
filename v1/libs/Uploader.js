const formidable = require('formidable');
const path = require('path');
const fs = require('fs');
const makeDir = require('make-dir');

// images uploader
module.exports.createImage = function(req) {
  return new Promise((resolve, reject) => {
    let form = new formidable.IncomingForm();
    let upload;
    let fileName;
    let filesObj;
    let imagePath;
    let pathMinus;
    let pathUpload;
    let imagePathArray = [];
    const separator = !(process.env.NODE_ENV === 'production') ? '../' : '/';

    form.parse(req, function(err, fields, files) {
      if (err) {
        return next(err);
      }
      let pathName = fields.path || req.query.path;
      upload = pathName
        ? path.join(separator, 'upload', 'img', pathName)
        : path.join(separator, 'upload', 'img');
      pathMinus = path.join(process.cwd(), separator, 'upload');
      filesObj = files;

      form.uploadDir = path.join(process.cwd(), upload);
      pathUpload = form.uploadDir;

      makeDir(pathUpload).then(() => {
        if (Object.keys(filesObj).length) {
          Object.keys(filesObj).forEach((file, index, array) => {
            fileName = path.join(pathUpload, filesObj[file].name);

            const preheader =
              process.env.NODE_ENV === 'production'
                ? 'https://api-shuvalovo.tw1.ru'
                : '';
            imagePath = `${preheader}${fileName.substr(pathMinus.length)}`;
            imagePathArray.push(imagePath);

            fs.rename(filesObj[file].path, fileName, function(err) {
              if (err) {
                console.error(err);
                fs.unlinkSync(fileName);
                fs.rename(filesObj[file].path, fileName);
              }

              if (index === array.length - 1) {
                if (array.length === 1) {
                  resolve(imagePathArray[0]);
                } else {
                  resolve(imagePathArray);
                }
              }
            });
          });
        } else reject({ error: 'not a single image passed' });
      });
    });
  });
};
