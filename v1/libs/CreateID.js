module.exports.CreateID = shema => {
  return new Promise((resolve, reject) => {
    shema
      .find({})
      .limit(1)
      .sort({ id: -1 })
      .then(data => {
        let id;
        if (!data.length) {
          id = 1;
        } else {
          id = Number(data[0].id) + 1;
        }
        resolve(id);
      })
      .catch(error => {
        reject(error);
      });
  });
};
