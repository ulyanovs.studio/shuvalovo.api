const bCrypt = require('bcryptjs');

module.exports.createHash = password => {
  return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

module.exports.isValidPassword = (user, password) => {
  return bCrypt.compareSync(password, user);
};
