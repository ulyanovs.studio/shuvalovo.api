const db = require('../models/db/User');
const passwordLibs = require('../libs/PasswordBcrypt');
const schema = require('../models/Schema');
const uuidv4 = require('uuidv4');
const pagination = require('../libs/Pagination');
const newID = require('../libs/CreateID').CreateID;
const auth = require('../middleware/Auth');

// start of use mongoose DB SCHEME
const schemaUsers = schema.User;

// password operations
const createHash = passwordLibs.createHash;
const isValidPassword = passwordLibs.isValidPassword;

// controllers for POST request /api/v~/login
module.exports.login = function(req, res) {
  let bodyObj = req.body;
  schemaUsers
    .findOne({ email: bodyObj.email })
    .then(user => {
      if (user && isValidPassword(user.password, bodyObj.password)) {
        db.getUser(user.id).then(item => {
          if (user.active) {
            res.status(200).json({
              user: item,
              cookies: {
                access_token: user.access_token,
                refresh_token: Buffer.from(
                  `${createHash(user.access_token)}|${Date.now() + 604800000}`
                ).toString('base64')
              }
            });
          } else {
            res.status(404).json({ error: 'user not found' });
          }
        });
      } else {
        res.status(404).json({ error: 'user not found' });
      }
    })
    .catch(error => {
      res.status(400).json({
        error: 'user not found! check email or password.'
      });
    });
};

// controllers for POST auto request /api/authFromToken
// replaces the old token with a new one
// for authentication
module.exports.authFromToken = function(req, res) {
  const tokenObj = req.body;
  if (tokenObj.access_token) {
    let accessHash = Buffer.from(req.body.refresh_token, 'base64')
      .toString('ascii')
      .split('|')[0];
    schemaUsers
      .findOne(
        { access_token: tokenObj.access_token },
        { password: false, __v: false, access_token: false, _id: false }
      )
      .then(user => {
        if (
          user &&
          user.active &&
          accessHash &&
          isValidPassword(accessHash, tokenObj.access_token)
        ) {
          res.status(200).json(user);
        } else if (
          !accessHash ||
          !isValidPassword(accessHash, tokenObj.access_token)
        ) {
          let newToken = {
            access_token: uuidv4()
          };
          db.updateUserToken(newToken, user.id).then(() => {
            res.status(400).json({ error: 'autorization falled' });
          });
        } else {
          res.status(404).json({ error: 'token not found' });
        }
      })
      .catch(err => {
        res.status(400).json({ error: 'autorization falled' });
      });
  } else {
    res.status(400).json({ error: 'autorization falled' });
  }
};

// controllers for GET request /api/v~/admins
module.exports.getAdmins = function(req, res) {
  db.getAdmins(req.query)
    .then(results => {
      schemaUsers.count({ role: 'admin' }).then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'admins'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/users
module.exports.getUsers = function(req, res) {
  db.getUsers(req.query)
    .then(results => {
      schemaUsers.count({ role: 'user' }).then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'users'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/users/:id
module.exports.getUser = function(req, res) {
  db.getUser(req.params.id)
    .then(results => {
      if (results) {
        res.json(results);
      } else {
        res.status(404).json({ error: 'user not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for POST request /api/v~/users
module.exports.saveNewUser = function(req, res) {
  const bodyObj = req.body;
  bodyObj.password
    ? (bodyObj.password = createHash(bodyObj.password))
    : (bodyObj.password = null);

  let cookies = [];
  let token;
  if (req.headers) {
    if (req.headers.cookie) {
      cookies = req.headers.cookie.split(' ');
    }
  }
  if (cookies.length) {
    let index = cookies.findIndex(item => item.includes('__a_t'));
    let cookiesToken = cookies[index].split('=')[1];
    token = cookiesToken.substr(0, cookiesToken.length - 1);
  }

  let saveNewUser = (id) => {
    db.saveNewUser(bodyObj, id)
      .then(results => {
        db.getUser(results.id).then(newUser => {
          res.status(201).json({
            user: newUser,
            cookies: {
              access_token: results.access_token,
              refresh_token: Buffer.from(
                `${createHash(results.access_token)}|${Date.now() + 604800000}`
              ).toString('base64')
            }
          });
        });
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  };

  newID(schemaUsers)
    .then(id => {
      schemaUsers
        .findOne({ email: bodyObj.email })
        .then(user => {
          if (user) {
            res
              .status(400)
              .json({ error: 'user with this login already exists' });
          } else {
            if (bodyObj.password) {
              if (!token && bodyObj.role === 'user') {
                saveNewUser(id);
              } else if (token && bodyObj.role === 'admin') {
                schemaUsers
                  .findOne({ access_token: token })
                  .then(user => {
                    if (user.role === 'admin') {
                      saveNewUser(id);
                    } else {
                      res.status(400).json({
                        error:
                          'The operation is prohibited, not enough rights to access'
                      });
                    }
                  })
                  .catch(err => {
                    res.status(400).json({
                      error:
                        'The operation is prohibited, not enough rights to access'
                    });
                  });
              } else if (token && bodyObj.role === 'user') {
                saveNewUser(id);
              } else {
                res.status(400).json({
                  error:
                    'The operation is prohibited, not enough rights to access'
                });
              }
            } else {
              res.status(400).json({ error: 'Password is required' });
            }
          }
        })
        .catch(err => {
          res.status(400).json({ error: err });
          console.log(err);
        });
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
};

// controllers for DELETE request /api/v~/users/:id
module.exports.deleteUser = function(req, res) {
  db.deleteUser(req.params.id)
    .then(results => {
      if (results) {
        res.json(results);
      } else {
        res.status(404).json({ error: 'user not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PUT request /api/updateUser/:id
module.exports.updateUser = function(req, res) {
  const bodyObj = req.body;
  let updateUser = user => {
    db.updateUser(bodyObj, user, req.params.id).then(results => {
      if (results) {
        db.getUser(results.id).then(item => {
          res.status(200).json(item);
        });
      } else {
        res.status(400).json({ error: 'user not found' });
      }
    });
  };
  schemaUsers
    .findOne({ id: req.params.id })
    .then(user => {
      if (user) {
        if (bodyObj.oldPassword && bodyObj.password) {
          if (isValidPassword(user.password, bodyObj.oldPassword)) {
            bodyObj.password = createHash(bodyObj.password);
            updateUser(user);
          } else {
            res.status(400).json({ error: 'password incorrect' });
          }
        } else if (bodyObj.password && user.role === 'admin') {
          bodyObj.password = createHash(bodyObj.password);
          updateUser(user);
        } else if (bodyObj.adminAction) {
          if (bodyObj.password) {
            bodyObj.password = createHash(bodyObj.password);
          }
          updateUser(user);
        } else if (bodyObj.oldPassword) {
          if (isValidPassword(user.password, bodyObj.oldPassword)) {
            updateUser(user);
          } else {
            res.status(400).json({ error: 'password incorrect' });
          }
        } else {
          res.status(400).json({ error: 'password incorrect' });
        }
      } else {
        res.status(400).json({ error: 'undefined user' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
