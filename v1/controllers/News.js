const db = require('../models/db/News');
const schema = require('../models/Schema');
const pagination = require('../libs/Pagination');
const newID = require('../libs/CreateID').CreateID;

// start of use mongoose DB SCHEME
const schemaNews = schema.News;

// controllers for GET request /api/v~/news
module.exports.getNews = function(req, res) {
  db.getNews(req.query)
    .then(results => {
      schemaNews.count().then(count => {
        let data = pagination.paginationData(results, req.query, count, 'news');
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/news/public
module.exports.getNewsPublic = function(req, res) {
  db.getNewsPublic(req.query)
    .then(results => {
      schemaNews.count({ active: true }).then(count => {
        let data = pagination.paginationData(
            results,
            req.query,
            count,
            'news'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/news/:id
module.exports.getNewsItem = function(req, res) {
  db.getNewsItem(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'news not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for GET request /api/v~/news/:slug
module.exports.getNewsItemBySlug = function(req, res) {
  db.getNewsItemBySlug(req.params.slug)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'news not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for POST request /api/v~/news
module.exports.saveNewNews = function(req, res) {
  const bodyObj = req.body;
  newID(schemaNews)
    .then(id => {
      db.saveNewNews(bodyObj, id).then(results => {
        db.getNewsItem(results.id).then(news => {
          res.status(201).json(news);
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for DELETE request /api/v~/news/:id
module.exports.deleteNews = function(req, res) {
  db.deleteNews(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'news not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PATCH request /api/v~/news/:id
module.exports.updateNews = function(req, res) {
  const bodyObj = req.body;
  schemaNews
    .findOne({ id: req.params.id })
    .then(news => {
      if (news) {
        db.updateNews(bodyObj, news, req.params.id).then(results => {
          if (results) {
            db.getNewsItem(results.id).then(item => {
              res.status(200).json(item);
            });
          } else {
            res.status(400).json({ error: 'news not found' });
          }
        });
      } else {
        res.status(400).json({ error: 'undefined news' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
