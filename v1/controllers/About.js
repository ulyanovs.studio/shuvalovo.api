const dbNews = require('../models/db/News');
const dbVacancy = require('../models/db/Vacancy');
const dbProduct = require('../models/db/Product');

module.exports.getAboutPagePublic = async function(req, res) {
  let response = {};
  response.news = await dbNews.getNewsPublicSomeItems(4);
  response.vacancies = await dbVacancy.getVacanciesPublicAboutPage(4);
  response.products = await dbProduct.getProductsPublicAboutPage(4);
  try {
    res.status(200).json(response);
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
};
