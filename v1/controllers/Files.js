const uploader = require('../libs/Uploader');
const path = require('path');
const fs = require('fs');
const host = 'https://api-shuvalovo.tw1.ru';

// controllers for POST request /api/v~/news
module.exports.saveNewImage = function(req, res) {
  uploader
    .createImage(req)
    .then(data => {
      res.status(200).json({ photo: data });
    })
    .catch(err => {
      res.status(400).json(err);
    });
};

module.exports.deleteImage = function(req, res) {
  const separator = !(process.env.NODE_ENV === 'production') ? '../' : '/';
  let imagePath = !(process.env.NODE_ENV === 'production')
    ? req.query.photo
    : req.query.photo.substr(host.length);
  let filePath = path.join(process.cwd(), separator, 'upload', imagePath);
  fs.unlink(filePath, err => {
    if (err) {
      res.status(400).json(err);
    }
    res.status(200).json({ photo: req.body.photo });
  });
};
