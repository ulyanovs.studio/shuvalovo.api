const db = require('../models/db/Product');
const dbCategories = require('../models/db/Category');
const schema = require('../models/Schema');
const pagination = require('../libs/Pagination');
const newID = require('../libs/CreateID').CreateID;

// start of use mongoose DB SCHEME
const schemaProduct = schema.Product;

// controllers for GET request /api/v~/products
module.exports.getProducts = function(req, res) {
  db.getProducts(req.query)
    .then(results => {
      schemaProduct.count().then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'products'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/products/public
module.exports.getProductsPublic = function(req, res) {
  db.getProductsPublic(req.query)
    .then(products => {
      let response = {};
      dbCategories.getCategoriesPublic().then(categories => {
        let activeCategory = [];
        let productsInActiveCategory = [];
        if (categories.length) {
          categories.forEach(item => {
            activeCategory.push(item.slug);
          });
        }
        if (products.length && activeCategory.length) {
          products.forEach(item => {
            if (activeCategory.includes(item.category)) {
              productsInActiveCategory.push(item);
            }
          });
        }
        response.categories = categories;
        response.products = productsInActiveCategory;
        schemaProduct.count({ active: true }).then(count => {
          let data = pagination.paginationData(
              response,
              req.query,
              count,
              'data'
          );
          res.status(200).json(data);
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/products/public/:category
module.exports.getProductsPublicSortCategory = function(req, res) {
  db.getProductsPublicSortCategory(req.query)
    .then(products => {
      let response = {};
      response.products = products;
      dbCategories.getCategoriesPublic().then(categories => {
        response.categories = categories;
        schemaProduct.count({ active: true, category: req.query.category }).then(count => {
          let data = pagination.paginationData(
              response,
              req.query,
              count,
              'data'
          );
          res.status(200).json(data);
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/products/:id
module.exports.getProductsItem = function(req, res) {
  db.getProductsItem(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'product not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for GET request /api/v~/products/:slug
module.exports.getProductsItemBySlug = function(req, res) {
  db.getProductsItemBySlug(req.params.slug)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'product not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for POST request /api/v~/products
module.exports.saveNewProduct = function(req, res) {
  const bodyObj = req.body;
  newID(schemaProduct)
    .then(id => {
      db.saveNewProduct(bodyObj, id).then(results => {
        db.getProductsItem(results.id).then(product => {
          dbCategories
            .getCategoriesItemSlug(product.category)
            .then(category => {
              let newCategory = category;
              if (!newCategory.productsCount) {
                newCategory.productsCount = 0;
              }
              newCategory.productsCount++;
              dbCategories
                .updateCategory(newCategory, category, category.id)
                .then(() => {
                  res.status(201).json(product);
                });
            });
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for DELETE request /api/v~/products/:id
module.exports.deleteProduct = function(req, res) {
  db.deleteProduct(req.params.id)
    .then(results => {
      if (results) {
        dbCategories.getCategoriesItemSlug(results.category).then(category => {
          let newCategory = category;
          if (newCategory.productsCount > 0) {
            newCategory.productsCount--;
          }
          dbCategories
            .updateCategory(newCategory, category, category.id)
            .then(() => {
              res.status(200).json(results);
            });
        });
      } else {
        res.status(404).json({ error: 'product not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PATCH request /api/v~/products/:id
module.exports.updateProduct = function(req, res) {
  const bodyObj = req.body;
  schemaProduct
    .findOne({ id: req.params.id })
    .then(product => {
      if (product) {
        let oldCategory = product.category;
        db.updateProduct(bodyObj, product, req.params.id).then(results => {
          if (results) {
            db.getProductsItem(results.id).then(item => {
              if (oldCategory === item.category) {
                res.status(200).json(item);
              } else {
                let categoryPlus = false;

                let updateCategory = categoryItem => {
                  return new Promise(resolve => {
                    dbCategories
                      .getCategoriesItemSlug(categoryItem)
                      .then(category => {
                        console.log(category, categoryItem);
                        let newCategory;
                        if (category) {
                          newCategory = category;
                        } else {
                          newCategory = 0;
                        }
                        if (newCategory.productsCount > 0 && !categoryPlus) {
                          newCategory.productsCount--;
                        }
                        if (categoryPlus) {
                          newCategory.productsCount++;
                        }
                        dbCategories
                          .updateCategory(newCategory, category, category.id)
                          .then(() => {
                            resolve();
                          });
                      });
                  });
                };

                updateCategory(oldCategory).then(() => {
                  categoryPlus = true;
                  updateCategory(item.category).then(() => {
                    res.status(200).json(item);
                  });
                });
              }
            });
          } else {
            res.status(400).json({ error: 'product not found' });
          }
        });
      } else {
        res.status(400).json({ error: 'undefined product' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
