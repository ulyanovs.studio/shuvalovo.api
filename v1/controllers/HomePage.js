const db = require('../models/db/Pages');
const dbNews = require('../models/db/News');
const schema = require('../models/Schema');

// start of use mongoose DB SCHEME
const schemaHomePage = schema.HomePage;

// controllers for GET request /api/v~/home
module.exports.getHomePage = function(req, res) {
  db.getHomePage()
    .then(results => {
      res.json(results);
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/home/public
module.exports.getHomePagePublic = function(req, res) {
  db.getHomePagePublic()
    .then(pages => {
      dbNews.getNewsPublicSomeItems(4).then(news => {
        let home = pages[0];
        let response = {
          page: home,
          news: news
        };
        res.status(200).json(response);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for POST request /api/v~/home
module.exports.saveHomePage = function(req, res) {
  const bodyObj = req.body;
  db.saveHomePage(bodyObj)
    .then(results => {
      res.status(201).json(results);
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for DELETE request /api/v~/home/:id
module.exports.deleteHomePage = function(req, res) {
  db.deleteHomePage(req.query.id)
    .then(results => {
      if (results) {
        res.json(results);
      } else {
        res.status(400).json({ error: 'user not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PATCH request /api/v~/home
module.exports.updateHomePage = function(req, res) {
  const bodyObj = req.body;
  schemaHomePage
    .find({})
    .then(pages => {
      if (pages[0]) {
        db.updateHomePage(bodyObj, pages[0], pages[0]._id).then(results => {
          if (results) {
            res.json(results);
          } else {
            res.status(400).json({ error: 'user not found' });
          }
        });
      } else {
        res.status(400).json({ error: 'undefined user' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
