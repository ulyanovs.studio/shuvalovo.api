const db = require('../models/db/Vacancy');
const schema = require('../models/Schema');
const pagination = require('../libs/Pagination');
const newID = require('../libs/CreateID').CreateID;

// start of use mongoose DB SCHEME
const schemaVacancy = schema.Vacancy;

// controllers for GET request /api/v~/vacancies
module.exports.getVacancies = function(req, res) {
  db.getVacancies(req.query)
    .then(results => {
      schemaVacancy.count().then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'vacancies'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/vacancies/public
module.exports.getVacanciesPublic = function(req, res) {
  db.getVacanciesPublic(req.query)
    .then(results => {
      schemaVacancy.count({ active: true }).then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'vacancies'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/vacancies/:id
module.exports.getVacanciesItem = function(req, res) {
  db.getVacanciesItem(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'vacancy not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for GET request /api/v~/vacancies/:slug
module.exports.getVacanciesItemBySlug = function(req, res) {
  db.getVacanciesItemBySlug(req.params.slug)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'vacancy not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for POST request /api/v~/vacancies
module.exports.saveNewVacancy = function(req, res) {
  const bodyObj = req.body;
  newID(schemaVacancy)
    .then(id => {
      db.saveNewVacancy(bodyObj, id).then(results => {
        db.getVacanciesItem(results.id).then(vacancy => {
          res.status(201).json(vacancy);
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for DELETE request /api/v~/vacancies/:id
module.exports.deleteVacancy = function(req, res) {
  db.deleteVacancy(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'vacancy not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PATCH request /api/v~/news/:id
module.exports.updateVacancy = function(req, res) {
  const bodyObj = req.body;
  schemaVacancy
    .findOne({ id: req.params.id })
    .then(news => {
      if (news) {
        db.updateVacancy(bodyObj, news, req.params.id).then(results => {
          if (results) {
            db.getVacanciesItem(results.id).then(item => {
              res.status(200).json(item);
            });
          } else {
            res.status(400).json({ error: 'vacancy not found' });
          }
        });
      } else {
        res.status(400).json({ error: 'undefined vacancy' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
