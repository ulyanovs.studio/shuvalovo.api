const db = require('../models/db/Category');
const schema = require('../models/Schema');
const pagination = require('../libs/Pagination');
const newID = require('../libs/CreateID').CreateID;

// start of use mongoose DB SCHEME
const schemaCategory = schema.Category;
const schemaProduct = schema.Product;
// controllers for GET request /api/v~/categories
module.exports.getCategories = function(req, res) {
  db.getCategories(req.query)
    .then(results => {
      schemaCategory.count().then(count => {
        let data = pagination.paginationData(
          results,
          req.query,
          count,
          'categories'
        );
        res.status(200).json(data);
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/categories/product
module.exports.getCategoriesProduct = function(req, res) {
  db.getCategoriesProduct(req.query)
    .then(results => {
      res.status(200).json(results);
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/categories/public
module.exports.getCategoriesPublic = function(req, res) {
  db.getCategoriesPublic(req.query)
    .then(results => {
      res.status(200).json(results);
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for GET request /api/v~/home/categories
module.exports.getHomeCategories = async function(req, res) {
  let categories = await db.getCategoriesPublic(req.query);
  let products = [];
  let counter = 0;
  let productValue = async () => {
    if (categories.length) {
      let product = await schemaProduct
        .find(
          { active: true, category: categories[counter].slug },
          { __v: false, _id: false, description: false }
        )
        .sort({ sort: -1, id: -1 })
        .limit(1);
      products.push(product[0]);
      counter++;
      if (counter < categories.length) {
        await productValue();
      }
    }
  };
  await productValue();
  try {
    res.status(200).json({
      categories: categories,
      products: products
    });
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

// controllers for GET request /api/v~/categories/:id
module.exports.getCategoriesItem = function(req, res) {
  db.getCategoriesItem(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'category not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for POST request /api/v~/categories
module.exports.saveNewCategory = function(req, res) {
  const bodyObj = req.body;
  newID(schemaCategory)
    .then(id => {
      db.saveNewCategory(bodyObj, id).then(results => {
        db.getCategoriesItem(results.id).then(category => {
          res.status(201).json(category);
        });
      });
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};

// controllers for DELETE request /api/v~/products/:id
module.exports.deleteCategory = function(req, res) {
  db.deleteCategory(req.params.id)
    .then(results => {
      if (results) {
        res.status(200).json(results);
      } else {
        res.status(404).json({ error: 'category not found' });
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message });
    });
};

// controllers for PATCH request /api/v~/news/:id
module.exports.updateCategory = function(req, res) {
  const bodyObj = req.body;
  schemaCategory
    .findOne({ id: req.params.id })
    .then(product => {
      if (product) {
        db.updateCategory(bodyObj, product, req.params.id).then(results => {
          if (results) {
            db.getCategoriesItem(results.id).then(item => {
              res.status(200).json(item);
            });
          } else {
            res.status(400).json({ error: 'category not found' });
          }
        });
      } else {
        res.status(400).json({ error: 'undefined category' });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message });
    });
};
