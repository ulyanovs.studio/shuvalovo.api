const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// data userScheme types
const userScheme = new Schema({
  id: {
    type: Number,
    required: [true, 'id user is required'],
    unique: true
  },
  access_token: {
    type: String,
    required: [true, 'access_token is required'],
    unique: true
  },
  email: {
    type: String,
    required: [true, 'email is required'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'password is required']
  },
  role: {
    type: String,
    required: [true, 'role is required']
  },
  firstName: {
    type: String,
    default: ''
  },
  lastName: {
    type: String,
    default: ''
  },
  middleName: {
    type: String,
    default: ''
  },
  phone: {
    type: String,
    default: ''
  },
  photo: {
    type: String,
    default: ''
  },
  active: {
    type: Boolean,
    default: false
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});

// data pageScheme types
const homePageScheme = new Schema({
  heading: {
    type: String,
    default: ''
  },
  text: {
    type: String,
    default: ''
  },
  photo: {
    type: String,
    default: ''
  },
  promoTextColor: {
    type: String,
    default: ''
  },
  promoBackgroundColor: {
    type: String,
    default: ''
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});

// data newsScheme types
const newsScheme = new Schema({
  id: {
    type: Number,
    required: [true, 'id news is required'],
    unique: true
  },
  heading: {
    type: String,
    required: [true, 'heading news is required']
  },
  description: {
    type: String,
    default: ''
  },
  text: {
    type: String,
    default: ''
  },
  slug: {
    type: String,
    default: ''
  },
  photos: {
    type: Array,
    default: []
  },
  cover: {
    type: String,
    default: ''
  },
  active: {
    type: Boolean,
    default: false
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});

// data productScheme types
const productScheme = new Schema({
  id: {
    type: Number,
    required: [true, 'id product is required'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'name product is required'],
    unique: true
  },
  category: { type: String },
  composition: {
    type: String,
    required: [true, 'composition product is required']
  },
  description: {
    type: String,
    default: ''
  },
  photo: {
    type: String,
    default: ''
  },
  slug: {
    type: String,
    default: ''
  },
  squirrels: {
    type: String,
    default: ''
  },
  fat: {
    type: String,
    default: ''
  },
  carbohydrates: {
    type: String,
    default: ''
  },
  energy: {
    type: String,
    default: ''
  },
  sort: {
    type: Number,
    default: 100
  },
  active: {
    type: Boolean,
    default: false
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});

// data categoryScheme types
const categoryScheme = new Schema({
  id: {
    type: Number,
    required: [true, 'id product is required'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'name category is required'],
    unique: true
  },
  slug: {
    type: String,
    default: ''
  },
  sort: {
    type: Number,
    default: 100
  },
  productsCount: {
    type: Number,
    default: 0
  },
  active: {
    type: Boolean,
    default: false
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});


// data vacancyScheme types
const vacancyScheme = new Schema({
  id: {
    type: Number,
    required: [true, 'id vacancy is required'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'name vacancy is required'],
    unique: true
  },
  description: {
    type: String,
    default: ''
  },
  slug: {
    type: String,
    default: ''
  },
  sort: {
    type: Number,
    default: 100
  },
  city: {
    type: String,
    default: ''
  },
  active: {
    type: Boolean,
    default: false
  },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed }
});

// exports Scheme models
module.exports.Vacancy = mongoose.model('vacancy', vacancyScheme);
module.exports.User = mongoose.model('users', userScheme);
module.exports.HomePage = mongoose.model('homepage', homePageScheme);
module.exports.News = mongoose.model('news', newsScheme);
module.exports.Product = mongoose.model('product', productScheme);
module.exports.Category = mongoose.model('category', categoryScheme);
