const schema = require('../Schema');

// start of use mongoose DB SCHEME
const schemaHomePage = schema.HomePage;

// GET pages of DB
module.exports.getHomePage = function() {
  return schemaHomePage.find({}, { __v: false, _id: false });
};

module.exports.getHomePagePublic = function() {
  return schemaHomePage.find({}, { __v: false, _id: false, updated_at: false });
};

// SAVE new page in DB
module.exports.saveHomePage = function(data) {
  const Page = new schemaHomePage({
    heading: data.heading,
    text: data.text,
    photo: data.photo,
    promoTextColor: data.promoTextColor,
    promoBackgroundColor: data.promoBackgroundColor
  });
  return Page.save();
};

// DELETE page of DB
module.exports.deleteHomePage = function(data) {
  return schemaHomePage.findOneAndRemove({ _id: data });
};

// UPDATE page in DB
module.exports.updateHomePage = function(data, page, id) {
  const Page = {};
  Page.heading = data.heading ? data.heading : page.heading;
  Page.text = data.text ? data.text : page.text;
  Page.photo = data.photo ? data.photo : page.photo;
  Page.promoTextColor = data.promoTextColor
    ? data.promoTextColor
    : page.promoTextColor;
  Page.promoBackgroundColor = data.promoBackgroundColor
    ? data.promoBackgroundColor
    : page.promoBackgroundColor;
  Page.updated_at = +new Date();
  return schemaHomePage.findOneAndUpdate(
    {
      _id: id
    },
    {
      $set: Page
    },
    { new: true }
  );
};
