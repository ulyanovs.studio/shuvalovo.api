const schema = require('../Schema');
const slug = require('limax');
const sortAndPagination = require('../../libs/Pagination').sortAndPagination;

// start of use mongoose DB SCHEME
const schemaVacancy = schema.Vacancy;

// verifying the validity of the VACANCY
const isNotValidVacancy = data => {
  return !data.name;
};

// GET VACANCY of DB
module.exports.getVacancies = function(data) {
  let pagination = sortAndPagination(data);
  return schemaVacancy
    .find({}, { __v: false, _id: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

module.exports.getVacanciesPublic = function(data) {
  let pagination = sortAndPagination(data);
  return schemaVacancy
    .find({ active: true }, { __v: false, _id: false, updated_at: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort({ sort: -1, id: -1 });
};

module.exports.getVacanciesPublicAboutPage = function(count) {
  return schemaVacancy
    .find(
      { active: true },
      {
        __v: false,
        _id: false,
        updated_at: false
      }
    )
    .limit(count)
    .sort({ sort: -1, id: -1 });
};

// GET VACANCY of DB
module.exports.getVacanciesItem = function(data) {
  return schemaVacancy.findOne({ id: data }, { __v: false, _id: false });
};

// GET VACANCY by slug of DB
module.exports.getVacanciesItemBySlug = function(data) {
  return schemaVacancy.findOne(
    { slug: data },
    { __v: false, _id: false, updated_at: false }
  );
};

// SAVE VACANCY product in DB
module.exports.saveNewVacancy = function(data, id) {
  if (isNotValidVacancy(data)) {
    return Promise.reject(new Error('name is required!'));
  }
  const vacancy = new schemaVacancy({
    id: id,
    name: data.name,
    description: data.description || '',
    city: data.city || '',
    slug: slug(data.name),
    sort: data.sort ? data.sort : 100,
    active: data.active || data.active === false ? data.active : true,
    created_at: +new Date()
  });
  return vacancy.save();
};

// DELETE VACANCY of DB
module.exports.deleteVacancy = function(data) {
  return schemaVacancy.findOneAndRemove({ id: data });
};

// UPDATE VACANCY in DB
module.exports.updateVacancy = function(data, vacancy, id) {
  const Vacancy = {};
  Vacancy.name = data.name ? data.name : vacancy.name;
  Vacancy.description = data.description
    ? data.description
    : vacancy.description;
  Vacancy.slug = data.name ? slug(data.name) : vacancy.slug;
  Vacancy.sort = data.sort ? data.sort : vacancy.sort;
  Vacancy.city = data.city || data.city === '' ? data.city : vacancy.city;
  Vacancy.active =
    data.active || data.active === false ? data.active : vacancy.active;
  Vacancy.created_at = data.created_at ? data.created_at : vacancy.created_at;
  Vacancy.updated_at = +new Date();
  return schemaVacancy.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: Vacancy
    },
    { new: true }
  );
};
