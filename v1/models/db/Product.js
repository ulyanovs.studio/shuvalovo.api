const schema = require('../Schema');
const slug = require('limax');
const sortAndPagination = require('../../libs/Pagination').sortAndPagination;

// start of use mongoose DB SCHEME
const schemaProduct = schema.Product;

// verifying the validity of the PRODUCT
const isNotValidProduct = data => {
  return !data.name || !data.category || !data.composition;
};

// GET Product of DB
module.exports.getProducts = function(data) {
  let pagination = sortAndPagination(data);
  return schemaProduct
    .find({}, { __v: false, _id: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

module.exports.getProductsPublic = function(data) {
  let pagination = sortAndPagination(data);
  return schemaProduct
    .find({ active: true }, { __v: false, _id: false, description: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort({ sort: -1, id: -1 });
};

module.exports.getProductsPublicSortCategory = function(data) {
  let pagination = sortAndPagination(data);
  return schemaProduct
    .find(
      { active: true, category: data.category },
      { __v: false, _id: false, description: false }
    )
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort({ sort: -1, id: -1 });
};

module.exports.getProductsPublicAboutPage = function(count) {
  return schemaProduct
    .find(
      { active: true },
      {
        __v: false,
        _id: false,
        updated_at: false
      }
    )
    .limit(count)
    .sort({ sort: -1, id: -1 });
};

// GET Product of DB
module.exports.getProductsItem = function(data) {
  return schemaProduct.findOne({ id: data }, { __v: false, _id: false });
};

// GET Product by slug of DB
module.exports.getProductsItemBySlug = function(data) {
  return schemaProduct.findOne({ slug: data }, { __v: false, _id: false });
};

// SAVE Product product in DB
module.exports.saveNewProduct = function(data, id) {
  if (isNotValidProduct(data)) {
    return Promise.reject(
      new Error('name, category, composition is required!')
    );
  }
  const product = new schemaProduct({
    id: id,
    name: data.name,
    category: data.category,
    composition: data.composition,
    photo: data.photo,
    description: data.description,
    squirrels: data.squirrels,
    fat: data.fat,
    carbohydrates: data.carbohydrates,
    energy: data.energy,
    slug: slug(data.name),
    sort: data.sort ? data.sort : 100,
    active: data.active || data.active === false ? data.active : true,
    created_at: +new Date()
  });
  return product.save();
};

// DELETE Product of DB
module.exports.deleteProduct = function(data) {
  return schemaProduct.findOneAndRemove({ id: data });
};

// UPDATE Product in DB
module.exports.updateProduct = function(data, product, id) {
  const Product = {};
  Product.name = data.name ? data.name : product.name;
  Product.category = data.category ? data.category : product.category;
  Product.composition = data.composition
    ? data.composition
    : product.composition;
  Product.photo = data.photo ? data.photo : product.photo;
  Product.squirrels = data.squirrels ? data.squirrels : product.squirrels;
  Product.fat = data.fat ? data.fat : product.fat;
  Product.carbohydrates = data.carbohydrates
    ? data.carbohydrates
    : product.carbohydrates;
  Product.energy = data.energy ? data.energy : product.energy;
  Product.description = data.description
    ? data.description
    : product.description;
  Product.slug = data.name ? slug(data.name) : product.slug;
  Product.sort = data.sort ? data.sort : product.sort;
  Product.active =
    data.active || data.active === false ? data.active : product.active;
  Product.updated_at = +new Date();
  return schemaProduct.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: Product
    },
    { new: true }
  );
};
