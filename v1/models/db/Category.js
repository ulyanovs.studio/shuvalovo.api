const schema = require('../Schema');
const slug = require('limax');
const sortAndPagination = require('../../libs/Pagination').sortAndPagination;

// start of use mongoose DB SCHEME
const schemaCategory = schema.Category;

// verifying the validity of the Category
const isNotValidCategory = data => {
  return Boolean(data.name);
};

// GET categories of DB
module.exports.getCategories = function(data) {
  let pagination = sortAndPagination(data);
  return schemaCategory
    .find({}, { __v: false, _id: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

module.exports.getCategoriesProduct = function(data) {
  return schemaCategory
      .find({}, { __v: false, _id: false })
      .sort(data);
};

module.exports.getCategoriesPublic = function(data) {
  return schemaCategory
    .find(
      { active: true, productsCount: { $gte: 1 } },
      { __v: false, _id: false }
    )
    .sort({ sort: -1, id: -1 });
};

// GET Category of DB
module.exports.getCategoriesItem = function(data) {
  return schemaCategory.findOne({ id: data }, { _id: false, __v: false });
};

module.exports.getCategoriesItemSlug = function(data) {
  return schemaCategory.findOne({ slug: data }, { _id: false, __v: false });
};

// SAVE new Category in DB
module.exports.saveNewCategory = function(data, id) {
  if (!isNotValidCategory(data)) {
    return Promise.reject(new Error('name is required!'));
  }
  const Category = new schemaCategory({
    id: id,
    name: data.name,
    slug: slug(data.name),
    sort: data.sort || 100,
    productsCount: 0,
    active: data.active || data.active === false ? data.active : true,
    created_at: +new Date()
  });
  return Category.save();
};

// DELETE Category of DB
module.exports.deleteCategory = function(data) {
  return schemaCategory.findOneAndRemove({ id: data });
};

// UPDATE Category in DB
module.exports.updateCategory = function(data, category, id) {
  const Category = {};
  Category.name = data.name ? data.name : category.name;
  Category.slug = data.name ? slug(data.name) : category.slug;
  Category.sort = data.sort ? data.sort : category.sort;
  Category.productsCount = data.productsCount
    ? data.productsCount
    : category.productsCount;
  Category.active =
    data.active || data.active === false ? data.active : category.active;
  Category.updated_at = +new Date();
  return schemaCategory.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: Category
    },
    { new: true }
  );
};
