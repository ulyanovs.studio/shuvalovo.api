const uuidv1 = require('uuid/v1');
const schema = require('../Schema');
const sortAndPagination = require('../../libs/Pagination').sortAndPagination;

// start of use mongoose DB SCHEME
const schemaUsers = schema.User;

// verifying the validity of the USER
const isNotValidUser = data => {
  return data.email && data.password && data.role;
};

// GET users of DB
module.exports.getAdmins = function(data) {
  let pagination = sortAndPagination(data);
  return schemaUsers
    .find(
      { role: 'admin' },
      { password: false, __v: false, access_token: false, _id: false }
    )
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

module.exports.getUsers = function(data) {
  let pagination = sortAndPagination(data);
  return schemaUsers
    .find(
      { role: 'user' },
      { password: false, __v: false, access_token: false, _id: false }
    )
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

// GET user of DB
module.exports.getUser = function(data) {
  return schemaUsers.findOne(
    { id: data },
    { password: false, __v: false, access_token: false, _id: false }
  );
};

// SAVE new user in DB
module.exports.saveNewUser = function(data, id) {
  if (!isNotValidUser(data)) {
    return Promise.reject(new Error('email, role, password is required!'));
  }
  const User = new schemaUsers({
    id: id,
    access_token: uuidv1(),
    email: data.email,
    password: data.password,
    firstName: data.firstName,
    lastName: data.lastName,
    middleName: data.middleName,
    phone: data.phone,
    role: data.role,
    active: data.active || data.active === false ? data.active : true,
    created_at: +new Date()
  });
  return User.save();
};

// DELETE user of DB
module.exports.deleteUser = function(data) {
  return schemaUsers.findOneAndRemove({ id: data });
};

// UPDATE user in DB
module.exports.updateUser = function(data, user, id) {
  const User = {};
  User.email = data.email ? data.email : user.email;
  User.password = data.password ? data.password : user.password;
  User.role = data.role ? data.role : user.role;
  User.firstName =
    data.firstName || data.firstName === '' ? data.firstName : user.firstName;
  User.lastName =
    data.lastName || data.lastName === '' ? data.lastName : user.lastName;
  User.middleName =
    data.middleName || data.middleName === ''
      ? data.middleName
      : user.middleName;
  User.phone = data.phone || data.phone === '' ? data.phone : user.phone;
  User.active =
    data.active || data.active === false ? data.active : user.active;
  User.updated_at = +new Date();
  return schemaUsers.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: User
    },
    { new: true }
  );
};

module.exports.updateUserToken = function(data, id) {
  const User = {};
  User.access_token = data.access_token;
  User.updated_at = +new Date();
  return schemaUsers.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: User
    },
    { new: true }
  );
};
