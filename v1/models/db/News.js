const schema = require('../Schema');
const slug = require('limax');
const sortAndPagination = require('../../libs/Pagination').sortAndPagination;

// start of use mongoose DB SCHEME
const schemaNews = schema.News;

// verifying the validity of the NEWS
const isNotValidNews = data => {
  return !data.heading;
};

// GET news of DB
module.exports.getNews = function(data) {
  let pagination = sortAndPagination(data);
  return schemaNews
    .find({}, { __v: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort(pagination.sort);
};

module.exports.getNewsPublic = function(data) {
  let pagination = sortAndPagination(data);
  return schemaNews
    .find({ active: true }, { __v: false, _id: false })
    .skip(pagination.perPage * pagination.page - pagination.perPage)
    .limit(pagination.perPage)
    .sort({ id: -1 });
};

module.exports.getNewsPublicSomeItems = function(count) {
  return schemaNews
    .find(
      { active: true },
      {
        __v: false,
        _id: false,
        text: false,
        updated_at: false,
        description: false,
        photos: false
      }
    )
    .limit(count || 4)
    .sort({ id: -1 });
};

// GET news of DB
module.exports.getNewsItem = function(data) {
  return schemaNews.findOne({ id: data }, { __v: false });
};

// GET news by slug of DB
module.exports.getNewsItemBySlug = function(data) {
  return schemaNews.findOne({ slug: data }, { __v: false });
};

// SAVE new news in DB
module.exports.saveNewNews = function(data, id) {
  if (isNotValidNews(data)) {
    return Promise.reject(new Error('heading is required!'));
  }
  const News = new schemaNews({
    id: id,
    heading: data.heading,
    description: data.description,
    text: data.text,
    cover: data.cover,
    photos: data.photos,
    slug: slug(data.heading),
    active: data.active || data.active === false ? data.active : true,
    created_at: +new Date()
  });
  return News.save();
};

// DELETE news of DB
module.exports.deleteNews = function(data) {
  return schemaNews.findOneAndRemove({ id: data });
};

// UPDATE news in DB
module.exports.updateNews = function(data, news, id) {
  const News = {};
  News.heading = data.heading ? data.heading : news.heading;
  News.description =
    data.description || data.description === ''
      ? data.description
      : news.description;
  News.text = data.text ? data.text : news.text;
  News.cover = data.cover ? data.cover : news.cover;
  News.photos = data.photos ? data.photos : news.photos;
  News.slug = data.heading ? slug(data.heading) : news.slug;
  News.active =
    data.active || data.active === false ? data.active : news.active;
  News.created_at = data.created_at ? data.created_at : news.created_at;
  News.updated_at = +new Date();
  return schemaNews.findOneAndUpdate(
    {
      id: id
    },
    {
      $set: News
    },
    { new: true }
  );
};
