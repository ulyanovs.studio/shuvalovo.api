const { Router } = require('express');
const router = Router();

const ctrlUser = require('../controllers/User');
const ctrlHome = require('../controllers/HomePage');
const ctrlNews = require('../controllers/News');
const ctrlProduct = require('../controllers/Product');
const ctrlVacancy = require('../controllers/Vacancy');
const ctrlCategory = require('../controllers/Category');
const ctrlAbout = require('../controllers/About');
const ctrlFiles = require('../controllers/Files');
const auth = require('../middleware/Auth');
const baseUrl = '/api/v1';

/*** users ***/
router
  .get(`${baseUrl}/admins`, auth.admin, ctrlUser.getAdmins)
  .get(`${baseUrl}/users`, auth.admin, ctrlUser.getUsers)
  .get(`${baseUrl}/users/:id`, auth.allUser, ctrlUser.getUser)
  .post(`${baseUrl}/users`, ctrlUser.saveNewUser)
  .patch(`${baseUrl}/users/:id`, auth.allUser, ctrlUser.updateUser)
  .delete(`${baseUrl}/users/:id`, auth.admin, ctrlUser.deleteUser);

/*** users login ***/
router
  .post(`${baseUrl}/login`, ctrlUser.login)
  .post(`${baseUrl}/authFromToken`, ctrlUser.authFromToken);

/*** pages ***/
router
  .get(`${baseUrl}/home`, auth.admin, ctrlHome.getHomePage)
  .get(`${baseUrl}/home/public`, ctrlHome.getHomePagePublic)
  .get(`${baseUrl}/home/categories`, ctrlCategory.getHomeCategories)
  .post(`${baseUrl}/home`, auth.admin, ctrlHome.saveHomePage)
  .patch(`${baseUrl}/home`, auth.admin, ctrlHome.updateHomePage)
  .delete(`${baseUrl}/home/:id`, auth.admin, ctrlHome.deleteHomePage);

/*** news ***/
router
  .get(`${baseUrl}/news`, auth.admin, ctrlNews.getNews)
  .get(`${baseUrl}/news/public`, ctrlNews.getNewsPublic)
  .get(`${baseUrl}/news/:id`, auth.admin, ctrlNews.getNewsItem)
  .get(`${baseUrl}/news/slug/:slug`, ctrlNews.getNewsItemBySlug)
  .post(`${baseUrl}/news`, auth.admin, ctrlNews.saveNewNews)
  .patch(`${baseUrl}/news/:id`, auth.admin, ctrlNews.updateNews)
  .delete(`${baseUrl}/news/:id`, auth.admin, ctrlNews.deleteNews);

/*** product ***/
router
  .get(`${baseUrl}/products`, auth.admin, ctrlProduct.getProducts)
  .get(`${baseUrl}/products/public`, ctrlProduct.getProductsPublic)
  .get(`${baseUrl}/products/public/sort-category`, ctrlProduct.getProductsPublicSortCategory)
  .get(`${baseUrl}/products/:id`, auth.admin, ctrlProduct.getProductsItem)
  .get(`${baseUrl}/products/slug/:slug`, ctrlProduct.getProductsItemBySlug)
  .post(`${baseUrl}/products`, auth.admin, ctrlProduct.saveNewProduct)
  .patch(`${baseUrl}/products/:id`, auth.admin, ctrlProduct.updateProduct)
  .delete(`${baseUrl}/products/:id`, auth.admin, ctrlProduct.deleteProduct);

/*** vacancies ***/
router
  .get(`${baseUrl}/vacancies`, auth.admin, ctrlVacancy.getVacancies)
  .get(`${baseUrl}/vacancies/public`, ctrlVacancy.getVacanciesPublic)
  .get(`${baseUrl}/vacancies/:id`, auth.admin, ctrlVacancy.getVacanciesItem)
  .get(`${baseUrl}/vacancies/slug/:slug`, ctrlVacancy.getVacanciesItemBySlug)
  .post(`${baseUrl}/vacancies`, auth.admin, ctrlVacancy.saveNewVacancy)
  .patch(`${baseUrl}/vacancies/:id`, auth.admin, ctrlVacancy.updateVacancy)
  .delete(`${baseUrl}/vacancies/:id`, auth.admin, ctrlVacancy.deleteVacancy);

/*** about page ***/
router
    .get(`${baseUrl}/about/public`, ctrlAbout.getAboutPagePublic);

/*** categories ***/
router
  .get(`${baseUrl}/categories`, auth.admin, ctrlCategory.getCategories)
  .get(`${baseUrl}/categories/product`, ctrlCategory.getCategoriesProduct)
  .get(`${baseUrl}/categories/public`, ctrlCategory.getCategoriesPublic)
  .get(`${baseUrl}/categories/:id`, auth.admin, ctrlCategory.getCategoriesItem)
  .post(`${baseUrl}/categories`, auth.admin, ctrlCategory.saveNewCategory)
  .patch(`${baseUrl}/categories/:id`, auth.admin, ctrlCategory.updateCategory)
  .delete(`${baseUrl}/categories/:id`, auth.admin, ctrlCategory.deleteCategory);

/*** img ***/
router
  .post(`${baseUrl}/img`, ctrlFiles.saveNewImage)
  .delete(`${baseUrl}/img`, ctrlFiles.deleteImage);

module.exports = router;
