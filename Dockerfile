FROM node:carbon AS base

RUN mkdir -p /root/api-shuvalovo
COPY . /root/api-shuvalovo

RUN mkdir -p /root/api-shuvalovo/upload

WORKDIR /root/api-shuvalovo

COPY package*.json ./

RUN npm ci && npm i -g pm2

ENV NODE_ENV=production

COPY . /root/api-shuvalovo

ENV HOST 0.0.0.0
EXPOSE 5000

CMD pm2 startOrRestart ecosystem.json --no-daemon
