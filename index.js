const express = require('express');
const consola = require('consola');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 5000;
const helmet = require('helmet');
require('./v1/models/Index');
const routes = require('./v1/routes/Index');
const cors = require('cors');
const path = require('path');

const app = express();

// setting various HTTP headers
app.use(helmet());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(cookieParser());

app.use(express.static(path.join(process.cwd(), 'upload')));

app.use(routes);

// error processing
app.use((req, res, next) => {
  res.status(404).json({ err: `404 | Not found` });
});

app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).json({ err: '500 | Server error' });
});

app.set('port', port);

// Listen the server
app.listen(port, host);
consola.ready({
  message: `Server listening on http://${host}:${port}`,
  badge: true
});
